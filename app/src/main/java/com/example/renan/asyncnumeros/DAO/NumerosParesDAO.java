package com.example.renan.asyncnumeros.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.renan.asyncnumeros.helpers.SQLiteHelper;
import com.example.renan.asyncnumeros.model.NumerosPares;

import java.util.ArrayList;
import java.util.List;

public class NumerosParesDAO {

    private Context context;
    private SQLiteDatabase db;
    private String[] colunas = new String[]{"numero"};
    private SQLiteHelper helper;

    public NumerosParesDAO(Context context) {
        this.context = context;
        helper = new SQLiteHelper(this.context, "numerospares", null, 1);
        db = helper.getWritableDatabase();
    }

    public List<NumerosPares> retornarTodos() {
        Cursor cursor = db.query("numerospares", colunas, null, null, null, null, "numero");
        List<NumerosPares> numerosParesList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (cursor.moveToNext()) {
                NumerosPares numerosPares = new NumerosPares();
                numerosPares.setNumero(cursor.getInt(0));
                numerosParesList.add(numerosPares);
            }
        }
        return numerosParesList;
    }

    public void deleteAll(){
        db.delete("numerospares",null,null);
    }

    public boolean numeroJaExiste(int numero){
        Cursor cursor = db.query("numerospares", colunas,"numero = ?",new String[]{String.valueOf(numero)},null,null,null);
        return cursor.moveToFirst();
    }

    public void insert(int numero) {
        ContentValues valores = new ContentValues();
        valores.put("numero", numero);
        long retorno = db.insert("numerospares", null, valores);
        if (retorno == -1) {
            Toast.makeText(context, "Houve um problema ao inserir o numero!", Toast.LENGTH_SHORT).show();
        }
    }
}
