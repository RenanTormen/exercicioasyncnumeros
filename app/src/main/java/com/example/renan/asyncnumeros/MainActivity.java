package com.example.renan.asyncnumeros;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.renan.asyncnumeros.DAO.NumerosParesDAO;
import com.example.renan.asyncnumeros.model.NumerosPares;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button btnIniciar;
    private ListView listNumeros = null;
    private TextView txtProgresso;
    private ProgressBar barraDeProgresso;
    private NumerosParesDAO numerosParesDAO = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnIniciar = findViewById(R.id.btnIniciar);
        listNumeros = findViewById(R.id.listNumeros);
        txtProgresso = findViewById(R.id.textProgresso);
        barraDeProgresso = findViewById(R.id.mainProgress);
        numerosParesDAO = new NumerosParesDAO(this);

        barraDeProgresso.setVisibility(View.INVISIBLE);
        atualizarListaDeNumeros();
    }

    private void popularListView() {
        List<NumerosPares> listaDeNumeros = numerosParesDAO.retornarTodos();
        ArrayAdapter<NumerosPares> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, listaDeNumeros);
        listNumeros.setAdapter(adapter);
    }

    public void atualizarListaDeNumeros() {
        popularListView();
    }

    public void iniciarProcesso(View v) {
        numerosParesDAO.deleteAll();
        new TaskInsercao().execute(1);
    }

    class TaskInsercao extends AsyncTask<Integer, Integer, String> {

        @Override
        protected void onPreExecute() {
            barraDeProgresso.setVisibility(View.VISIBLE);
            Toast.makeText(MainActivity.this, "Iniciando o processo", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            txtProgresso.setText("Inseriu o numero : " + values[0]);
            barraDeProgresso.setProgress(values[0] % 100);
        }

        @Override
        protected void onPostExecute(String s) {
            txtProgresso.setText(s);
            barraDeProgresso.setVisibility(View.INVISIBLE);
            atualizarListaDeNumeros();
        }

        @Override
        protected String doInBackground(Integer... integers) {
            int valor = 1;
            for (int i = 0; i != 1000; i++) {
                int numeroInserir = 0;

                if(valor % 2 == 0){
                    numerosParesDAO.insert(valor);
                }
                valor = valor + 1;
                publishProgress(i);
            }
            return "Processo Finalizado!";
        }
    }

}

