package com.example.renan.asyncnumeros.model;

public class NumerosPares {
    private int numero;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }


    @Override
    public String toString() {
        return String.valueOf(this.numero);
    }
}
